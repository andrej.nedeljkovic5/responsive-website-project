import React from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import Navbar from './Components/Navbar';
import Cover from './Components/Cover';

const App = () => {
  return (
    <>
    <BrowserRouter>
      {/*<Navbar/>*/}
      <Cover/>
    </BrowserRouter>
    </>
  );
}

export default App;
