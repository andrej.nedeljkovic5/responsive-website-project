import React, { useEffect, useState } from 'react'
import  {Link}  from 'react-router-dom'
import '../Components/CSS/navbar.css'
import logo from '../Components/Images/logo-01-01.png'

const Navbar = () => {

  const [isClicked, setIsClicked] = useState(false)
  const [sticky, setSticky] = useState(false)
  useEffect(() => {
    const handleScroll = () => {
      setSticky(window.scrollY > 0);
    };
    window.addEventListener('scroll', handleScroll)
  }, [])

  return (
    <nav className= {`navbar ${sticky? "sticky" : ""} ${isClicked && "activeNav"}`}>
      <div className='logo'>
        <img src={logo} alt = 'logo'/>
      </div>
      <div className='toggle-button' onClick={() => setIsClicked(!isClicked)}>
        <span className={`bar ${isClicked && `active1`}`}></span>
        <span className={`bar ${isClicked && `active2`}`}></span>
        <span className={`bar ${isClicked && `active3`}`}></span>
      </div>
      <div className={`navbar-links ${isClicked && `active`}`}>
        <ul>
          <li><Link to='/'><p>O nama</p></Link></li>
          <li><Link to='/'><p>Skola fudbala</p></Link></li>
          <li><Link to='/'><p>Rodjendani</p></Link></li>
          <li><Link to='/'><p>Kontakt</p></Link></li>
        </ul>
      </div>
    </nav>
  )
}

export default Navbar
