import React from 'react'
import './CSS/cover.css'
import Navbar from './Navbar'
import { Fade } from 'react-reveal'

const Cover = () => {
  return (
      <section id='cover'>
        <Navbar/>
        <div className = 'header-text'>
        <Fade bottom>
          <div>
            <h1>Zlatna Lopta</h1>
            <p>Balon za fudbal</p>
          </div>
        </Fade>
        </div>
        <div className='neki'></div>
      </section>
  )
}

export default Cover
